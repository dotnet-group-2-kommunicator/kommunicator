USE [master]
GO
/****** Object:  Database [KommunicatorDb]    Script Date: 28/10/2020 12:36:05 ******/
CREATE DATABASE [KommunicatorDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'KommunicatorDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\KommunicatorDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'KommunicatorDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\KommunicatorDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [KommunicatorDb] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [KommunicatorDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [KommunicatorDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [KommunicatorDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [KommunicatorDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [KommunicatorDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [KommunicatorDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [KommunicatorDb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [KommunicatorDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [KommunicatorDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [KommunicatorDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [KommunicatorDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [KommunicatorDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [KommunicatorDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [KommunicatorDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [KommunicatorDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [KommunicatorDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [KommunicatorDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [KommunicatorDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [KommunicatorDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [KommunicatorDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [KommunicatorDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [KommunicatorDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [KommunicatorDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [KommunicatorDb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [KommunicatorDb] SET  MULTI_USER 
GO
ALTER DATABASE [KommunicatorDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [KommunicatorDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [KommunicatorDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [KommunicatorDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [KommunicatorDb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [KommunicatorDb] SET QUERY_STORE = OFF
GO
USE [KommunicatorDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Administrator]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Administrator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AdminEmail] [nvarchar](max) NULL,
	[Super] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Administrator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Message]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[MessageTemplateId] [int] NOT NULL,
	[ResponseTemplateId] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MessageTemplate]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_MessageTemplate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Response]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Response](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseContent] [nvarchar](max) NULL,
	[UserEmail] [nvarchar](450) NULL,
	[MessageId] [int] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Response] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ResponseTemplate]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResponseTemplate](
	[ResponseTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
 CONSTRAINT [PK_ResponseTemplate] PRIMARY KEY CLUSTERED 
(
	[ResponseTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_SubCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Topic]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Topic](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SubCategoryID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Topic] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TopicSubscription]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TopicSubscription](
	[TopicID] [int] NOT NULL,
	[UserEmail] [nvarchar](450) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_TopicSubscription] PRIMARY KEY CLUSTERED 
(
	[TopicID] ASC,
	[UserEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 28/10/2020 12:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserEmail] [nvarchar](450) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201007123207_inital Migration', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201007135519_update subcategory', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201008072024_adding user table', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201008074602_topicsubscription update', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201013083136_adding desktop database', N'3.1.9')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201022120624_updating template tables', N'3.1.9')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201023074640_update response template ', N'3.1.9')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201026095722_adding additional response option', N'3.1.9')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201026134804_update message table', N'3.1.9')
GO
SET IDENTITY_INSERT [dbo].[Administrator] ON 

INSERT [dbo].[Administrator] ([ID], [AdminEmail], [Super], [PasswordHash], [Active]) VALUES (1, N'mathiastb13@gmail.com', 1, N'blablabla', 1)
INSERT [dbo].[Administrator] ([ID], [AdminEmail], [Super], [PasswordHash], [Active]) VALUES (2, N'Adming@gmail.com', 1, N'blabla', 1)
SET IDENTITY_INSERT [dbo].[Administrator] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (1, N'Nature', 1)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (2, N'Technology', 1)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (3, N'Music', 1)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (4, N'Epic', 0)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (5, N'Geographic', 0)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (6, N'Sports', 1)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (7, N'Politics', 1)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (8, N'javell', 1)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (9, N'test', 0)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (10, N'kategori', 0)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (11, N'uhuh', 0)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (12, N'kjkjhkhkjhk', 0)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (13, NULL, 0)
INSERT [dbo].[Category] ([ID], [Name], [Active]) VALUES (14, N'fghg', 0)
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Message] ON 

INSERT [dbo].[Message] ([MessageTemplateId], [ResponseTemplateId], [Active], [MessageID], [MessageDescription]) VALUES (1, 1, 1, 1, N'first message descirption')
INSERT [dbo].[Message] ([MessageTemplateId], [ResponseTemplateId], [Active], [MessageID], [MessageDescription]) VALUES (2, 3, 1, 4, N'second message description')
INSERT [dbo].[Message] ([MessageTemplateId], [ResponseTemplateId], [Active], [MessageID], [MessageDescription]) VALUES (5, 21, 1, 5, N'This is a nice message')
INSERT [dbo].[Message] ([MessageTemplateId], [ResponseTemplateId], [Active], [MessageID], [MessageDescription]) VALUES (3, 21, 1, 6, N'sadasdasd')
INSERT [dbo].[Message] ([MessageTemplateId], [ResponseTemplateId], [Active], [MessageID], [MessageDescription]) VALUES (36, 22, 1, 7, N'awsdsad')
SET IDENTITY_INSERT [dbo].[Message] OFF
GO
SET IDENTITY_INSERT [dbo].[MessageTemplate] ON 

INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (1, N'Hello world', N'Alot of contetn', 1)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (2, N'message2', N'more content', 1)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (3, N'Standard outgoing ', N'<p>This is some input</p> <h1>this is a heading </h1>', 1)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (4, N'Standard asdfasdf', N'<p>This is some input</p>
<h1>this is a heading</h1>
<p>asdfasdf</p>', 0)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (5, N'fucka you', N'<h5>Alot of contetn</h5>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>This is content</p>', 0)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (6, N'this better work', N'<h5>yaya it works</h5>', 0)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (34, N'Standard outgoing 2', N'<p>This is some input</p>
<h1>this is a headingasdas</h1>', 0)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (35, N'Standard outgoing 3', N'<p>This is some dog shit</p>', 1)
INSERT [dbo].[MessageTemplate] ([ID], [Name], [Content], [Active]) VALUES (36, N'Standard outgoing 4', N'<p>You''re actually just dog shit</p>', 1)
SET IDENTITY_INSERT [dbo].[MessageTemplate] OFF
GO
SET IDENTITY_INSERT [dbo].[Response] ON 

INSERT [dbo].[Response] ([ID], [ResponseContent], [UserEmail], [MessageId], [Active]) VALUES (1, N'{responseType: ''text'', responseValue: ''Hello this is my response. Verry nice''}', N'magnus@gmail.com', 1, 1)
INSERT [dbo].[Response] ([ID], [ResponseContent], [UserEmail], [MessageId], [Active]) VALUES (2, N'{"responseType":"likert","responseValue":5}', N'magnus@gmail.com', 1, 1)
INSERT [dbo].[Response] ([ID], [ResponseContent], [UserEmail], [MessageId], [Active]) VALUES (3, N'{"responseType":"text","responseValue":"Halla! Jysla kult brev eg fikk av dokker!"}', N'magnus@gmail.com', 1, 1)
INSERT [dbo].[Response] ([ID], [ResponseContent], [UserEmail], [MessageId], [Active]) VALUES (4, N'{"responseType":"text","responseValue":"dfgdgsdf"}', N'magnus@gmail.com', 1, 1)
INSERT [dbo].[Response] ([ID], [ResponseContent], [UserEmail], [MessageId], [Active]) VALUES (5, N'{"responseType":"likert","responseValue":10}', N'magnus@gmail.com', 1, 1)
SET IDENTITY_INSERT [dbo].[Response] OFF
GO
SET IDENTITY_INSERT [dbo].[ResponseTemplate] ON 

INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (1, N'First Response', N'This is the first response')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (3, N'Second response', N'this is the second response')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (6, N'Second response', N'this is the second response')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (7, N'Second response', N'this is the second response')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (8, N'Second response', NULL)
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (10, N'Second response', N'hello')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (12, N'Second response', N'<p>hello mothofucka</p>
<p>&nbsp;</p>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (13, N'Second response', N'<p>asdfssss</p>
<p>&nbsp;</p>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (15, N'Second response', N'<ul style="list-style-type: circle;">
<li style="text-align: center;">lkajsdfljk</li>
</ul>
<p>&nbsp;</p>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (16, N'fuck you', N'<p style="text-align: center;"><strong>please work</strong></p>
<p style="text-align: center;">&nbsp;</p>
<ul>
<li style="text-align: left;"><strong>asdf</strong>
<ul>
<li style="text-align: left;"><strong>asd2</strong>
<ul>
<li style="text-align: left;"><strong>fucking</strong></li>
</ul>
</li>
</ul>
</li>
</ul>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (17, N'fuck you', N'<p style="text-align: center;"><strong>please work</strong></p>
<p style="text-align: center;">&nbsp;</p>
<ul>
<li style="text-align: left;"><strong>asdf</strong>
<ul>
<li style="text-align: left;"><strong>asd2</strong>
<ul>
<li style="text-align: left;"><strong>fucking</strong></li>
</ul>
</li>
</ul>
</li>
</ul>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (19, N'fuck you too', N'<p style="text-align: center;"><strong>yes I agree</strong></p>
<p style="text-align: center;">&nbsp;</p>
<ul>
<li style="text-align: left;"><strong>asdf</strong>
<ul>
<li style="text-align: left;"><strong>asd2</strong>
<ul>
<li style="text-align: left;"><strong>fucking</strong></li>
</ul>
</li>
</ul>
</li>
</ul>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (20, N'fuck you too', N'<p style="text-align: center;"><strong>yes I agree</strong></p>
<p style="text-align: center;">&nbsp;</p>
<ul>
<li style="text-align: left;"><strong>asdf</strong>
<ul>
<li style="text-align: left;"><strong>asd2</strong>
<ul>
<li style="text-align: left;"><strong>fucking</strong></li>
</ul>
</li>
</ul>
</li>
</ul>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (21, N'fuck you too', N'<p style="text-align: center;"><strong>yes I agree</strong></p>
<p style="text-align: center;">&nbsp;</p>
<ul>
<li style="text-align: left;"><strong>asdf</strong>
<ul>
<li style="text-align: left;"><strong>asd2</strong>
<ul>
<li style="text-align: left;"><strong>fucking</strong></li>
</ul>
</li>
</ul>
</li>
</ul>')
INSERT [dbo].[ResponseTemplate] ([ResponseTemplateID], [Name], [Content]) VALUES (22, N'First Response Extended', N'<p>This is not the first response (or is it?)</p>')
SET IDENTITY_INSERT [dbo].[ResponseTemplate] OFF
GO
SET IDENTITY_INSERT [dbo].[SubCategory] ON 

INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (1, 1, 1, N'Bees')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (2, 1, 1, N'Trees')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (3, 2, 1, N'Java')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (4, 2, 1, N'.Net Core')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (5, 4, 1, N'Epic stuff')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (6, 3, 1, N'Rap')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (7, 3, 1, N'Jazz')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (8, 7, 1, N'Trump 2020')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (9, 7, 1, N'Biden 2020')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (10, 6, 1, N'Hockey')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (11, 8, 0, N'hello ok')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (12, 5, 1, N'Switzerland')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (13, 10, 0, N'subcategory')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (14, 8, 0, N'fdsfsfddffsd')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (15, 1, 0, N'Sand')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (16, 11, 0, N'hjkh')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (17, 12, 0, N'gfdghh')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (18, 8, 0, NULL)
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (19, 14, 0, N'fgdhf')
INSERT [dbo].[SubCategory] ([ID], [CategoryID], [Active], [Name]) VALUES (20, 1, 1, N'Sand')
SET IDENTITY_INSERT [dbo].[SubCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Topic] ON 

INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (1, 1, N'Save the bees foundation', N'These guys save bees', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (2, 1, N'Beekeeping newsletters', N'The latest on beekeeping', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (3, 3, N'Intellij', N'Is Intellij the best IDE out there', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (4, 4, N'C# or Java', N'Which one is the big daddy of object oriented programming', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (5, 5, N'Epic stunts', N'See the most daring and epic stunds in mankinds history', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (6, 6, N'Mumble rap', N'Should these rappers stop ', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (7, 7, N'Smooth classics', N'Check out the smoothest jazz artists on the planets', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (8, 8, N'Elections', N'Is trump gonna knock out biden and take or will biden remember to show up', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (9, 9, N'Elections', N'sleepy joe might actually win', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (10, 10, N'Best sport', N'best sport period', 1)
INSERT [dbo].[Topic] ([ID], [SubCategoryID], [Name], [Description], [Active]) VALUES (12, 12, N'hockey legends', N'Look at these legends', 1)
SET IDENTITY_INSERT [dbo].[Topic] OFF
GO
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (1, N'a@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (1, N'mathiastb13@gmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (1, N'mrmartinsen.96@gmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (1, N'sven4696@gmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (1, N'tm22222@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (1, N'tm72123@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (2, N'a@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (2, N'sven4696@gmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (2, N'tm72123@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (3, N'a@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (3, N'andreas_myhr_93@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (3, N'sven4696@gmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (3, N'tm72123@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (4, N'a@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (4, N'sven4696@gmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (6, N'a@hotmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (6, N'sven4696@gmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (6, N'tm72123@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (7, N'a@hotmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (7, N'sven4696@gmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (8, N'a@hotmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (8, N'sven4696@gmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (8, N'tm22222@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (9, N'a@hotmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (9, N'sven4696@gmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (9, N'tm22222@hotmail.com', 1)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (10, N'a@hotmail.com', 0)
INSERT [dbo].[TopicSubscription] ([TopicID], [UserEmail], [Active]) VALUES (10, N'sven4696@gmail.com', 0)
GO
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'a@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas@gmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas_myhr_93@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas_myhr_93@hotmail.comm', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas_myhr_93123@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas_myhr_93234@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas_myhr_9344@hotmail.comm', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas_myhr_9377@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'andreas_myhr_93new@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'magnus@gmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'mathias', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'mathiastb13@gmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'mrmartinsen.96@gmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'sven4696@gmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'tm2@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'tm22222@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'tm7212@hotmail.com', 1)
INSERT [dbo].[User] ([UserEmail], [Active]) VALUES (N'tm72123@hotmail.com', 1)
GO
/****** Object:  Index [IX_Message_MessageTemplateId]    Script Date: 28/10/2020 12:36:07 ******/
CREATE NONCLUSTERED INDEX [IX_Message_MessageTemplateId] ON [dbo].[Message]
(
	[MessageTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Message_ResponseTemplateId]    Script Date: 28/10/2020 12:36:07 ******/
CREATE NONCLUSTERED INDEX [IX_Message_ResponseTemplateId] ON [dbo].[Message]
(
	[ResponseTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Response_MessageId]    Script Date: 28/10/2020 12:36:07 ******/
CREATE NONCLUSTERED INDEX [IX_Response_MessageId] ON [dbo].[Response]
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Response_UserEmail]    Script Date: 28/10/2020 12:36:07 ******/
CREATE NONCLUSTERED INDEX [IX_Response_UserEmail] ON [dbo].[Response]
(
	[UserEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_SubCategory_CategoryID]    Script Date: 28/10/2020 12:36:07 ******/
CREATE NONCLUSTERED INDEX [IX_SubCategory_CategoryID] ON [dbo].[SubCategory]
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Topic_SubCategoryID]    Script Date: 28/10/2020 12:36:07 ******/
CREATE NONCLUSTERED INDEX [IX_Topic_SubCategoryID] ON [dbo].[Topic]
(
	[SubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TopicSubscription_UserEmail]    Script Date: 28/10/2020 12:36:07 ******/
CREATE NONCLUSTERED INDEX [IX_TopicSubscription_UserEmail] ON [dbo].[TopicSubscription]
(
	[UserEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_MessageTemplate_MessageTemplateId] FOREIGN KEY([MessageTemplateId])
REFERENCES [dbo].[MessageTemplate] ([ID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_MessageTemplate_MessageTemplateId]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_ResponseTemplate_ResponseTemplateId] FOREIGN KEY([ResponseTemplateId])
REFERENCES [dbo].[ResponseTemplate] ([ResponseTemplateID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_ResponseTemplate_ResponseTemplateId]
GO
ALTER TABLE [dbo].[Response]  WITH CHECK ADD  CONSTRAINT [FK_Response_Message_MessageId] FOREIGN KEY([MessageId])
REFERENCES [dbo].[Message] ([MessageID])
GO
ALTER TABLE [dbo].[Response] CHECK CONSTRAINT [FK_Response_Message_MessageId]
GO
ALTER TABLE [dbo].[Response]  WITH CHECK ADD  CONSTRAINT [FK_Response_User_UserEmail] FOREIGN KEY([UserEmail])
REFERENCES [dbo].[User] ([UserEmail])
GO
ALTER TABLE [dbo].[Response] CHECK CONSTRAINT [FK_Response_User_UserEmail]
GO
ALTER TABLE [dbo].[SubCategory]  WITH CHECK ADD  CONSTRAINT [FK_SubCategory_Category_CategoryID] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[SubCategory] CHECK CONSTRAINT [FK_SubCategory_Category_CategoryID]
GO
ALTER TABLE [dbo].[Topic]  WITH CHECK ADD  CONSTRAINT [FK_Topic_SubCategory_SubCategoryID] FOREIGN KEY([SubCategoryID])
REFERENCES [dbo].[SubCategory] ([ID])
GO
ALTER TABLE [dbo].[Topic] CHECK CONSTRAINT [FK_Topic_SubCategory_SubCategoryID]
GO
ALTER TABLE [dbo].[TopicSubscription]  WITH CHECK ADD  CONSTRAINT [FK_TopicSubscription_Topic_TopicID] FOREIGN KEY([TopicID])
REFERENCES [dbo].[Topic] ([ID])
GO
ALTER TABLE [dbo].[TopicSubscription] CHECK CONSTRAINT [FK_TopicSubscription_Topic_TopicID]
GO
ALTER TABLE [dbo].[TopicSubscription]  WITH CHECK ADD  CONSTRAINT [FK_TopicSubscription_User_UserEmail] FOREIGN KEY([UserEmail])
REFERENCES [dbo].[User] ([UserEmail])
GO
ALTER TABLE [dbo].[TopicSubscription] CHECK CONSTRAINT [FK_TopicSubscription_User_UserEmail]
GO
USE [master]
GO
ALTER DATABASE [KommunicatorDb] SET  READ_WRITE 
GO
