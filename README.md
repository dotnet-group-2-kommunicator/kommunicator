# Kommunicator Backend

## Case Members:
 - Andreas Tønseth Myhr
 - Magnus Review Martinsen
 - Mathias Berntsen
 - Sven Daneel

## Case Mentors:
 - Dewald Els
 - Nicholas Lennox

## Technologies Used
### Frontend
	- Angular
### Backend
	- .NET Core
	- Entity Framework Core

## Description: 
The application can be compared to an email list where a user can subscribe to a interessted channel/topic, once a channel/topic has any updates or announcements the users will be notified. 
