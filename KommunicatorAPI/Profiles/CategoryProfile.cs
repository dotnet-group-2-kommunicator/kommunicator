﻿using AutoMapper;
using Kommunicator.DTOs;
using Kommunicator.DTOs.Category;
using Kommunicator.DTOs.SubCategory;
using Kommunicator.DTOs.Topic;
using Kommunicator.DTOs.TopicSubscription;
using Kommunicator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryDto>().ForMember(c => c.SubCategories, opt => opt.MapFrom(c => c.SubCategories));
            CreateMap<SubCategory, SingleSubCategoryDto>().ForMember(sc => sc.Topics, opt => opt.MapFrom(sc => sc.Topics));
            CreateMap<Topic, TopicDto>().ForMember(t => t.TopicSubscriptions, opt => opt.MapFrom(t => t.TopicSubscriptions));
            CreateMap<Topic, TopicDto>();
            CreateMap<TopicSubscription, TopicSubscriptionDto>();
            CreateMap<User, UserDto>().ForMember(u => u.TopicSubscription, opt => opt.MapFrom(u => u.TopicSubscriptions));
        }
    }
}
