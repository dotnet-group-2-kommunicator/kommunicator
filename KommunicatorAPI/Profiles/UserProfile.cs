﻿using AutoMapper;
using Kommunicator.DTOs.Category;
using Kommunicator.DTOs.SubCategory;
using Kommunicator.DTOs.Topic;
using Kommunicator.DTOs.TopicSubscription;
using Kommunicator.DTOs.User;
using Kommunicator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, SingleUserDto>().ForMember(u => u.TopicSubscriptions, opt => opt.MapFrom(u => u.TopicSubscriptions));
            CreateMap<Topic, UserTopicDto>().ForMember( t => t.Subcategory, opt => opt.MapFrom( t => t.SubCategory));
            CreateMap<SubCategory, userSubcategoryDto>().ForMember(s => s.Category, opt => opt.MapFrom(s => s.Category));
            CreateMap<Category, UserCategoryDto>();
            CreateMap<TopicSubscription, UserTopicSubscriptionDto>();
        }
    }
}
