﻿using Kommunicator.DTOs.TopicSubscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.User
{
    public class SingleUserDto
    {
        public string UserEmail { get; set; }

        public ICollection<UserTopicSubscriptionDto> TopicSubscriptions { get; set; }
    }
}
