﻿using Kommunicator.DTOs.TopicSubscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs
{
    public class UserDto
    {
        public string UserEmail { get; set; }

        public ICollection<TopicSubscriptionDto> TopicSubscription { get; set; }
    }
}
