﻿using Kommunicator.DTOs.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.SubCategory
{
    public class userSubcategoryDto
    {
        public string Name { get; set; }

        public UserCategoryDto Category { get; set; }
    }
}
