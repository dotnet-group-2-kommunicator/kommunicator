﻿using Kommunicator.DTOs.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.SubCategory
{
    public class SingleSubCategoryDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int categoryID { get; set; }
        public ICollection<TopicDto> Topics { get; set; }
    }
}
