﻿using Kommunicator.DTOs.TopicSubscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.Topic
{
    public class TopicDto
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public int SubCategoryID { get; set; }
        public string Description { get; set; }

        public ICollection<TopicSubscriptionDto> TopicSubscriptions { get; set; }
    }
}
