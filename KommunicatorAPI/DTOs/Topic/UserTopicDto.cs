﻿using Kommunicator.DTOs.SubCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.Topic
{
    public class UserTopicDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public userSubcategoryDto Subcategory { get; set; }

    }
}
