﻿using Kommunicator.DTOs.Topic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.TopicSubscription
{
    public class TopicSubscriptionDto
    {
        public TopicDto Topic { get; set; }

        [ForeignKey("UserDto")]
        public string UserEmail { get; set; }

        public Boolean active { get; set; }
        
    }
}
