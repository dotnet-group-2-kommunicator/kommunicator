﻿using Kommunicator.DTOs.SubCategory;
using Kommunicator.DTOs.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.TopicSubscription
{
    public class UserTopicSubscriptionDto
    {
        public int TopicID { get; set; }

        public UserTopicDto Topic { get; set; }
        public Boolean Active { get; set; }


    }
}
