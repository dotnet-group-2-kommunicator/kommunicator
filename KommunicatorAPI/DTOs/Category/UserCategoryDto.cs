﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.Category
{
    public class UserCategoryDto
    {
        public string Name { get; set; }
    }
}
