﻿using Kommunicator.DTOs.SubCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.DTOs.Category
{
    public class CategoryDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<SingleSubCategoryDto> SubCategories { get; set; }
    }
}
