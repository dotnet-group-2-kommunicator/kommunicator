﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class Message
    {
        public Message(string messageDescription, int messageTemplateId, int responseTemplateId, bool active)
        {
            MessageDescription = messageDescription;
            MessageTemplateId = messageTemplateId;
            ResponseTemplateId = responseTemplateId;
            Active = active;
        }

        [Key]
        public int MessageID { get; set; }

        public int MessageTemplateId { get; set; }

        public MessageTemplate MessageTemplate { get; set; }

        public int ResponseTemplateId { get; set; }

        public ResponseTemplate ResponseTemplate { get; set; }

        public string MessageDescription { get; set; }

        public bool Active { get; set; }


    }
}
