﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class EmailMessage
    {
        public List<string> Emails { get; set; }
        public List<string> UniqueLinks { get; set; }
        public List<Topic> Topics { get; set; }
        public string Subject { get; set; }
        public Message Message { get; set; }
    }
}
