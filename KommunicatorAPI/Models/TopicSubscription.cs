﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Kommunicator.Models
{
    public class TopicSubscription
    {
        public TopicSubscription(int id, bool active, string userEmail)
        {
            TopicID = id;
            Active = active;
            UserEmail = userEmail;
        }

        public TopicSubscription()
        {

        }

        // foreign key
        [Key]
        public int TopicID { get; set; }

        // Navigation property

        public Topic Topic { get; set; }

        public Boolean Active { get; set; }

        [ForeignKey("User")]
        public string UserEmail { get; set; }

        public User User { get; set; }


    }
}
