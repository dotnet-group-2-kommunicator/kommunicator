﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class SubCategory
    {
        public SubCategory( int categoryID, string name, Boolean active)
        {
            CategoryID = categoryID;
            Name = name;
            Active = active;
        }

        [Key]
        public int ID { get; set; }

        // Foreign key
        public int CategoryID { get; set; }

        public string Name { get; set; }

        public Boolean Active { get; set; }

        // Navigation property
        public Category Category { get; set; }
        // Collection of Topics

        public ICollection<Topic> Topics { get; set; }
    }
}
