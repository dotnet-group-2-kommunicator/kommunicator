﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class User
    {
        public User(string userEmail, bool active)
        {
            UserEmail = userEmail;
            Active = active;
        }

        [Key]
        public string UserEmail { get; set; }

        public Boolean Active { get; set; }

        // Navigation property

        public ICollection<TopicSubscription> TopicSubscriptions { get; set; }
    }
}
