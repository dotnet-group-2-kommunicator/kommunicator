﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class MessageTemplate
    {
        public MessageTemplate(string name, string content, bool active)
        {
            Name = name;
            Content = content;
            Active = active;
        }

        [Key]
        public int ID { get; set; }
       
        public string Name { get; set; }

        public string Content { get; set; }
        public bool Active { get; set; }

        public ICollection<Message> Messages { get; set; }

    }
}
