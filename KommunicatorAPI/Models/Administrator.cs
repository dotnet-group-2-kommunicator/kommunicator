﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class Administrator
    {

        public Administrator( string adminEmail, Boolean super, string passwordHash, Boolean active)
        {
            AdminEmail = adminEmail;
            Super = super;
            PasswordHash = passwordHash;
            Active = active;
        }
        [Key]
        public int ID { get; set; }

        public string AdminEmail { get; set; }
        
        public Boolean Super { get; set; }

        public string PasswordHash { get; set; }

        public Boolean Active { get; set; }


    }
}
