﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class Category
    {
        public Category(string name, bool active)
        {
            Name = name;
            Active = active;
        }

        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public Boolean Active { get; set; }

        // Collection of Subcategories 
        public ICollection<SubCategory> SubCategories { get; set; }




    }
}
