﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class ResponseTemplate
    {
        [Key]
        public int ResponseTemplateID { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public int ResponseType { get; set; }
    }
}
