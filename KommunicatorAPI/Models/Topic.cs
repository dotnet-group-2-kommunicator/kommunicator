﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class Topic
    {
        public Topic(int subCategoryID, string name, string description, bool active)
        {
            SubCategoryID = subCategoryID;
            Name = name;
            Description = description;
            Active = active;
        }

        [Key]
        public int ID { get; set; }

        public int SubCategoryID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Boolean Active { get; set; }

        // Navigation property
        public SubCategory SubCategory { get; set; }

        // Collection of topic subscriptions 
        public ICollection<TopicSubscription> TopicSubscriptions { get; set; }

    }
}
