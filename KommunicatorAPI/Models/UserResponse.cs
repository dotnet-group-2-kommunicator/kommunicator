﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator.Models
{
    public class UserResponse
    {
        public UserResponse(string responseContent, string userEmail, int messageId, bool active)
        {
            ResponseContent = responseContent;
            UserEmail = userEmail;
            MessageId = messageId;
            Active = active;
        }

        [Key]
        public int ID { get; set; }
        public string ResponseContent { get; set; }
        [ForeignKey("User")]
        public string UserEmail { get; set; }
        public User User { get; set; }
        public int MessageId { get; set; }
        public Message Message { get; set; }
        public bool Active { get; set; }
    }
}
