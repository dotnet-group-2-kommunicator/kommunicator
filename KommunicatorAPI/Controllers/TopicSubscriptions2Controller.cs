﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using Microsoft.AspNetCore.Authorization;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class TopicSubscriptions2Controller : ControllerBase
    {
        private readonly KommunicatorDbContext _context;

        public TopicSubscriptions2Controller(KommunicatorDbContext context)
        {
            _context = context;
        }

        // GET: api/TopicSubscriptions2
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TopicSubscription>>> GetTopicSubscription()
        {
            return await _context.TopicSubscription.ToListAsync();
        }

        // GET: api/TopicSubscriptions2/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TopicSubscription>> GetTopicSubscription(int id)
        {
            var topicSubscription = await _context.TopicSubscription.FindAsync(id);

            if (topicSubscription == null)
            {
                return NotFound();
            }

            return topicSubscription;
        }

        // PUT: api/TopicSubscriptions2/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTopicSubscription(int id, TopicSubscription topicSubscription)
        {
            if (id != topicSubscription.TopicID)
            {
                return BadRequest();
            }

            _context.Entry(topicSubscription).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicSubscriptionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TopicSubscriptions2
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TopicSubscription>> PostTopicSubscription(TopicSubscription topicSubscription)
        {
            var found = await _context.TopicSubscription.FindAsync(topicSubscription.TopicID, topicSubscription.UserEmail);

            if(found == null)
            {
                _context.TopicSubscription.Add(topicSubscription);

            } else 
            {
                found.Active = true;
                _context.Entry(found).State = EntityState.Modified;
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TopicSubscriptionExists(topicSubscription.TopicID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTopicSubscription", new { id = topicSubscription.TopicID }, topicSubscription);
        }

        // DELETE: api/TopicSubscriptions2/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TopicSubscription>> DeleteTopicSubscription(int id)
        {
            var topicSubscription = await _context.TopicSubscription.FindAsync(id);
            if (topicSubscription == null)
            {
                return NotFound();
            }

            _context.TopicSubscription.Remove(topicSubscription);
            await _context.SaveChangesAsync();

            return topicSubscription;
        }

        private bool TopicSubscriptionExists(int id)
        {
            return _context.TopicSubscription.Any(e => e.TopicID == id);
        }
    }
}
