using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using Services.MailService;
using System.Net;
using SendGrid.Helpers.Mail;
using SendGrid;
using Microsoft.AspNetCore.Authorization;

namespace Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmailController :ControllerBase
    {
        private readonly IMailService _mail;

        public EmailController(IMailService mail)
        {
            _mail = mail;
        }

        [HttpPost]
        public async Task<Response> Send(EmailMessage emailMessage)
        {
            return await _mail.SendOutgoingSet(emailMessage);
        }
    }
}