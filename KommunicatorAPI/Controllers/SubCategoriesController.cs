﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using Microsoft.AspNetCore.Authorization;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubCategoriesController : ControllerBase
    {
        private readonly KommunicatorDbContext _context;

        public SubCategoriesController(KommunicatorDbContext context)
        {
            _context = context;
        }

        // GET: api/SubCategories
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubCategory>>> GetSubCategory()
        {
            return await _context.SubCategory.ToListAsync();
        }

        // GET: api/SubCategories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubCategory>> GetSubCategory(int id)
        {
            var subCategory = await _context.SubCategory.FindAsync(id);

            if (subCategory == null)
            {
                return NotFound();
            }

            return subCategory;
        }

        // PUT: api/SubCategories/5
        [HttpPut("{id}")]
        public async Task<ActionResult<SubCategory>> PutSubCategory(int id, SubCategory subCategory)
        {
            if (id != subCategory.ID)
            {
                return BadRequest();
            }

            _context.Entry(subCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(subCategory);
        }

        // POST: api/SubCategories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<SubCategory>> PostSubCategory(SubCategory subCategory)
        {
            _context.SubCategory.Add(subCategory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubCategory", new { id = subCategory.ID }, subCategory);
        }

        /// <summary>
        /// Sets the active flag to false, thereby "Deleting" it and wont display to the user anymore
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // DELETE: api/SubCategories/5
        [HttpPut("delete/{id}")]
        public async Task<ActionResult<SubCategory>> DeleteSubCategory(int id)
        {
            var subCategory = await _context.SubCategory.Include(sc => sc.Topics).Where(sc => sc.ID == id).SingleAsync();
            subCategory.Active = false; 
            if (subCategory == null)
            {
                return NotFound();
            }

            var topics = subCategory.Topics;

            foreach (var topic in topics)
            {
                topic.Active = false;
                _context.Entry(topic).State = EntityState.Modified;
            }

            _context.Entry(subCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Sets the active flag of a subcategory to true, thereby retrieving it again
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // DELETE: api/SubCategories/5
        [HttpPut("retrieve/{id}")]
        public async Task<ActionResult<SubCategory>> RetrieveSubCategory(int id)
        {
            var subCategory = await _context.SubCategory.FindAsync(id);
            subCategory.Active = true;

            var topics = subCategory.Topics;

            foreach (var topic in topics)
            {
                topic.Active = false;
                _context.Entry(topic).State = EntityState.Modified;
            }
            if (subCategory == null)
            {
                return NotFound();
            }

            _context.Entry(subCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool SubCategoryExists(int id)
        {
            return _context.SubCategory.Any(e => e.ID == id);
        }
    }
}
