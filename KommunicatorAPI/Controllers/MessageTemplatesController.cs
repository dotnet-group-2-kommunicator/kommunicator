﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using Microsoft.AspNetCore.Authorization;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MessageTemplatesController : ControllerBase
    {
        private readonly KommunicatorDbContext _context;

        public MessageTemplatesController(KommunicatorDbContext context)
        {
            _context = context;
        }

        // GET: api/MessageTemplates
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MessageTemplate>>> GetMessageTemplate()
        {
            return await _context.MessageTemplate.ToListAsync();
        }

        // GET: api/MessageTemplates/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MessageTemplate>> GetMessageTemplate(int id)
        {
            var messageTemplate = await _context.MessageTemplate.FindAsync(id);

            if (messageTemplate == null)
            {
                return NotFound();
            }

            return messageTemplate;
        }

        // PUT: api/MessageTemplates/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMessageTemplate(int id, MessageTemplate messageTemplate)
        {
            if (id != messageTemplate.ID)
            {
                return BadRequest();
            }

            _context.Entry(messageTemplate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageTemplateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MessageTemplates
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MessageTemplate>> PostMessageTemplate(MessageTemplate messageTemplate)
        {
            var found = await _context.MessageTemplate.FindAsync(messageTemplate.ID);

            MessageTemplate returnedTemplate = messageTemplate;
            
            if((found.Content == messageTemplate.Content && found.Name == messageTemplate.Name) || (found.Name == messageTemplate.Name && found.Content != messageTemplate.Content))
            {
                found.Name = messageTemplate.Name;
                found.Content = messageTemplate.Content;
                _context.Entry(found).State = EntityState.Modified;
            }
            else
            {
                MessageTemplate newTemplate = new MessageTemplate(messageTemplate.Name, messageTemplate.Content, true);

                //_context.Entry<MessageTemplate>(found).State = EntityState.Detached;
                _context.MessageTemplate.Add(newTemplate);

                returnedTemplate = newTemplate;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if(MessageTemplateExists(messageTemplate.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
             

            return CreatedAtAction("GetMessageTemplate", new { id = returnedTemplate.ID }, returnedTemplate);
        }

        // DELETE: api/MessageTemplates/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MessageTemplate>> DeleteMessageTemplate(int id)
        {
            var messageTemplate = await _context.MessageTemplate.FindAsync(id);
            if (messageTemplate == null)
            {
                return NotFound();
            }

            _context.MessageTemplate.Remove(messageTemplate);
            await _context.SaveChangesAsync();

            return messageTemplate;
        }

        private bool MessageTemplateExists(int id)
        {
            return _context.MessageTemplate.Any(e => e.ID == id);
        }
    }
}
