﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResponsesController : ControllerBase
    {
        private readonly KommunicatorDbContext _context;

        public ResponsesController(KommunicatorDbContext context)
        {
            _context = context;
        }

        // GET: api/Responses
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserResponse>>> GetResponse()
        {
            return await _context.Response.ToListAsync();
        }

        // GET: api/Responses/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserResponse>> GetResponse(int id)
        {
            var response = await _context.Response.FindAsync(id);

            if (response == null)
            {
                return NotFound();
            }

            return response;
        }

        // PUT: api/Responses/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutResponse(int id, UserResponse response)
        {
            if (id != response.ID)
            {
                return BadRequest();
            }

            _context.Entry(response).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResponseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Responses
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<UserResponse>> PostResponse(UserResponse response)
        {
            var found = await _context.Response.Where(r => r.UserEmail.Equals(response.UserEmail) && r.MessageId.Equals(response.MessageId)).SingleOrDefaultAsync();

            if (found != null && found.MessageId == response.MessageId && found.UserEmail == response.UserEmail)
            {
                found.ResponseContent = response.ResponseContent;
                _context.Entry(found).State = EntityState.Modified;
            } else
            {
                _context.Response.Add(response);
            }

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetResponse", new { id = response.ID }, response);
        }

        // DELETE: api/Responses/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserResponse>> DeleteResponse(int id)
        {
            var response = await _context.Response.FindAsync(id);
            if (response == null)
            {
                return NotFound();
            }

            _context.Response.Remove(response);
            await _context.SaveChangesAsync();

            return response;
        }

        private bool ResponseExists(int id)
        {
            return _context.Response.Any(e => e.ID == id);
        }
    }
}
