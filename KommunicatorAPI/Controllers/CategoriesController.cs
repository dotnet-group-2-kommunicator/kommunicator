﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using AutoMapper;
using Kommunicator.DTOs.Category;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly KommunicatorDbContext _context;
        private readonly IMapper _mapper;

        public CategoriesController(KommunicatorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Categories
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Category>>> GetCategory()
        {
            return await _context.Category.Where(c => c.Active).ToListAsync();
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDto>> GetCategory(int id)
        {
            var category = await _context.Category.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }
            CategoryDto dto = _mapper.Map<CategoryDto>(category);


            return dto;
        }
        /// <summary>
        /// Gets specific Categories with their respective sub-categories and their included topics and the people subscribed to those topics.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // GET: api/Categories/5
        [HttpGet("category/{id}")]
        public async Task<ActionResult<CategoryDto>> GetAllCategoriesWithTopicsAndSubscribers(int id)
        {
            var category = await _context.Category.Include(c => c.SubCategories).ThenInclude(c => c.Topics).ThenInclude(c => c.TopicSubscriptions).Where(c => c.ID == id).SingleAsync();

            if (category == null)
            {
                return NotFound();
            }

            CategoryDto dto = _mapper.Map<CategoryDto>(category);

            return dto;
        }

        /// <summary>
        /// Gets all Categories with their respective sub-categories and their included topics.
        /// </summary>
        /// <returns></returns>

        // GET: api/Categories/sub-categories/topics
        [HttpGet("sub-categories/topics")]
        public async Task<ActionResult<IEnumerable<CategoryDto>>> GetAllCategoriesWithTopics()
        {
            var categories = await _context.Category
                .Include(c => c.SubCategories)
                .ThenInclude(sc => sc.Topics)
                .ThenInclude(t => t.TopicSubscriptions)
                .Where(c => c.Active)

                .ToListAsync();

            foreach (Category category in categories)
            {
                var subCategories = category.SubCategories.Where(sc => sc.Active == true).ToList();
                category.SubCategories = subCategories;
            }

            foreach (Category category in categories)
            {
                foreach (SubCategory subCategory in category.SubCategories)
                {
                    var topics = subCategory.Topics.Where(t => t.Active == true).ToList();
                    subCategory.Topics = topics;
                }
            }

            if (categories == null)
            {
                return NotFound();
            }


            return Ok(categories.Select(c => _mapper.Map<CategoryDto>(c)).ToList());
        }

        /// <summary>
        /// Get all topics for a category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // GET: api/Categories/5
        [HttpGet("topic/{id}")]
        public async Task<ActionResult<Category>> GetAllCategoryTopics(int id)
        {
            var category = await _context.Category.Include(c => c.SubCategories).ThenInclude(c => c.Topics).Where(c => c.ID == id).SingleAsync();

            if (category == null)
            {
                return NotFound();
            }

            return category;
        }

        // GET: api/Categories/5
        [HttpGet("categorysub")]
        public async Task<ActionResult<IEnumerable<Category>>> GetAllCategoryTopic()
        {
            var category = await _context.Category.Include(c => c.SubCategories).ThenInclude(c => c.Topics).ToListAsync();

            if (category == null)
            {
                return NotFound();
            }

            return category;
        }



        // PUT: api/Categories/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<ActionResult<Category>> PutCategory(int id, Category category)
        {
            if (id != category.ID)
            {
                return BadRequest();
            }

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(category);
        }

        // POST: api/Categories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory(Category category)
        {
            _context.Category.Add(category);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCategory", new { id = category.ID }, category);
        }


        /// <summary>
        /// Setting the Active flag to flase, Thereby "Deleting" the category and its sub categories and topics
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Categories/5
        [HttpPut("delete/{id}")]
        public async Task<ActionResult<Category>> DeleteCategory(int id)
        {
            var category = await _context.Category.Include(c => c.SubCategories).ThenInclude(c => c.Topics).Where(c => c.ID == id).SingleAsync();
            category.Active = false;

            var subc = category.SubCategories;

            foreach (var subcategory in subc)
            {
                subcategory.Active = false;
                _context.Entry(subcategory).State = EntityState.Modified;

                foreach (var topic in subcategory.Topics)
                {
                    topic.Active = false;
                    _context.Entry(topic).State = EntityState.Modified;
                }
            }


            if (category == null)
            {
                return NotFound();
            }

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Setting the Active flag to true, Thereby "Retrieving" the category and its sub categories and topics
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Categories/retrieve/5
        [HttpPut("retrieve/{id}")]
        public async Task<ActionResult<Category>> RetrieveCategory(int id)
        {
            var category = await _context.Category.Include(c => c.SubCategories).ThenInclude(c => c.Topics).Where(c => c.ID == id).SingleAsync();
            category.Active = false;

            var subc = category.SubCategories;

            foreach (var subcategory in subc)
            {
                subcategory.Active = false;
                _context.Entry(subcategory).State = EntityState.Modified;

                foreach (var topic in subcategory.Topics)
                {
                    topic.Active = false;
                    _context.Entry(topic).State = EntityState.Modified;
                }
            }


            if (category == null)
            {
                return NotFound();
            }

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        private bool CategoryExists(int id)
        {
            return _context.Category.Any(e => e.ID == id);
        }
    }
}
