﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kommunicator.Controllers
{
    //[Authorize]
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public List<string> Hi()
        {
            return new List<string>
            {
                "Hello"
            };
        }

        [HttpGet("nauth")]
        public string Test()
        {
            return "Non authenticated";
        }

        [Authorize]
        [HttpGet("auth")]
        public string ATest()
        {
            return "Authenticated";
        }
    }
}
