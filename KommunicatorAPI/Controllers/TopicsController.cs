﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using AutoMapper;
using Kommunicator.DTOs.Topic;
using Microsoft.AspNetCore.Authorization;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TopicsController : ControllerBase
    {
        private readonly KommunicatorDbContext _context;
        private readonly IMapper _mapper; 

        public TopicsController(KommunicatorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        // GET: api/Topics
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Topic>>> GetTopic()
        {
            return await _context.Topic.Include(t => t.TopicSubscriptions).ThenInclude(t => t.Topic).Where(t => t.Active == true).ToListAsync();
        }

        // GET: api/Topics/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Topic>> GetTopic(int id)
        {
            var topic = await _context.Topic.FindAsync(id);

            if (topic == null)
            {
                return NotFound();
            }

            return topic;
        }

        /// <summary>
        /// Get all subscribers to a specific topic
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Topics/5
        [HttpGet("subscription/{id}")]
        public async Task<ActionResult<TopicDto>> GetTopicSubscriptions(int id)
        {
            var topic = await _context.Topic.Include(t => t.TopicSubscriptions).Where(t => t.ID == id).SingleAsync();

            if (topic == null)
            {
                return NotFound();
            }

            TopicDto dto = _mapper.Map<TopicDto>(topic);

            return dto;
        }

        // PUT: api/Topics/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<ActionResult<Topic>> PutTopic(int id, Topic topic)
        {
            if (id != topic.ID)
            {
                return BadRequest();
            }

            _context.Entry(topic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(topic);
        }

        // POST: api/Topics
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Topic>> PostTopic(Topic topic)
        {
            _context.Topic.Add(topic);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTopic", new { id = topic.ID }, topic);
        }

        // PUT: api/Topics/delete/5
        [HttpPut("delete/{id}")]
        public async Task<ActionResult<Topic>> DeleteTopic(int id)
        {
            var topic = await _context.Topic.FindAsync(id);
            topic.Active = false;

            if (topic == null)
            {
                return NotFound();
            }


            _context.Entry(topic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(topic);
        }

        // Update: api/Topics/5
        [HttpPut("/Retrieve/{id}")]
        public async Task<ActionResult<Topic>> RetrieveTopic(int id)
        {
            var topic = await _context.Topic.FindAsync(id);
            topic.Active = !topic.Active;
            if (topic == null)
            {
                return NotFound();
            }


            _context.Entry(topic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        private bool TopicExists(int id)
        {
            return _context.Topic.Any(e => e.ID == id);
        }
    }
}
