﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using Microsoft.AspNetCore.Authorization;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResponseTemplatesController : ControllerBase
    {
        private readonly KommunicatorDbContext _context;

        public ResponseTemplatesController(KommunicatorDbContext context)
        {
            _context = context;
        }

        // GET: api/ResponseTemplates
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ResponseTemplate>>> GetResponseTemplate()
        {
            return await _context.ResponseTemplate.ToListAsync();
        }

        // GET: api/ResponseTemplates/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ResponseTemplate>> GetResponseTemplate(int id)
        {
            var responseTemplate = await _context.ResponseTemplate.FindAsync(id);

            if (responseTemplate == null)
            {
                return NotFound();
            }

            return responseTemplate;
        }

        // PUT: api/ResponseTemplates/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutResponseTemplate(int id, ResponseTemplate responseTemplate)
        {
            if (id != responseTemplate.ResponseTemplateID)
            {
                return BadRequest();
            }

            _context.Entry(responseTemplate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResponseTemplateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ResponseTemplates
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ResponseTemplate>> PostResponseTemplate(ResponseTemplate responseTemplate)
        {
            var found = await _context.ResponseTemplate.FindAsync(responseTemplate.ResponseTemplateID);

            ResponseTemplate returnedTemplate = responseTemplate;

            if(found.Name == responseTemplate.Name && ((found.Content != responseTemplate.Content) || (found.ResponseType != responseTemplate.ResponseType)))
                /*(found.Content == responseTemplate.Content && found.Name == responseTemplate.Name && found.ResponseType == responseTemplate.ResponseType) || // Name = Same, Content = Same, Type = Same
                (found.Name == responseTemplate.Name && found.Content == responseTemplate.Content && found.ResponseType != responseTemplate.ResponseType) || // Name = Same, Content = Same, Type != Same
                (found.Name == responseTemplate.Name && found.Content != responseTemplate.Content && found.ResponseType != responseTemplate.ResponseType) || // Name = Same, Content != Same, Type != Same
                (found.Name == responseTemplate.Name && found.Content != responseTemplate.Content && found.ResponseType == responseTemplate.ResponseType)) // Name = Same, Content != Same, Type = Same*/
            {
                found.Name = responseTemplate.Name;
                found.Content = responseTemplate.Content;
                found.ResponseType = responseTemplate.ResponseType;
                _context.Entry(found).State = EntityState.Modified;
            }
            else if (found.Name == responseTemplate.Name && found.Content == responseTemplate.Content && found.ResponseType == responseTemplate.ResponseType)
            {
                found.Name = responseTemplate.Name;
                found.Content = responseTemplate.Content;
                found.ResponseType = responseTemplate.ResponseType;
                _context.Entry(found).State = EntityState.Modified;
            }
            else
            {
                ResponseTemplate newTemplate = new ResponseTemplate()
                {
                    Name = responseTemplate.Name,
                    Content = responseTemplate.Content,
                    ResponseType = responseTemplate.ResponseType
                };

                _context.ResponseTemplate.Add(newTemplate);

                returnedTemplate = newTemplate;
            }

            try
            {
                await _context.SaveChangesAsync();

            } catch(DbUpdateException)
            {
                if (ResponseTemplateExists(responseTemplate.ResponseTemplateID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetResponseTemplate", new { id = returnedTemplate.ResponseTemplateID }, returnedTemplate);
        }

        // DELETE: api/ResponseTemplates/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ResponseTemplate>> DeleteResponseTemplate(int id)
        {
            var responseTemplate = await _context.ResponseTemplate.FindAsync(id);
            if (responseTemplate == null)
            {
                return NotFound();
            }

            _context.ResponseTemplate.Remove(responseTemplate);
            await _context.SaveChangesAsync();

            return responseTemplate;
        }

        private bool ResponseTemplateExists(int id)
        {
            return _context.ResponseTemplate.Any(e => e.ResponseTemplateID == id);
        }
    }
}
