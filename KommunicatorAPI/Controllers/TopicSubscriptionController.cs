﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Kommunicator;
using Kommunicator.Models;
using AutoMapper;
using Kommunicator.DTOs.Topic;
using Microsoft.AspNetCore.Authorization;

namespace Kommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TopicSubscriptionController : ControllerBase
    {
        private readonly KommunicatorDbContext _context;
        private readonly IMapper _mapper;

        public TopicSubscriptionController(KommunicatorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        // DELETE: api/Topics/5
        [HttpGet("{id}/{email}")]
        public async Task<ActionResult<TopicSubscription>> DeleteTopic(int id, string email)
        {
            var topic = await _context.TopicSubscription.Where( t=> t.TopicID.Equals(id) && t.UserEmail.Equals(email)).SingleOrDefaultAsync();
            topic.Active = !topic.Active;

            if (topic == null)
            {
                return NotFound();
            }


            _context.Entry(topic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        private bool TopicExists(int id)
        {
            return _context.Topic.Any(e => e.ID == id);
        }
    }
}
