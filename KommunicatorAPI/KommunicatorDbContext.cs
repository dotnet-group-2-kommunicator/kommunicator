﻿using Kommunicator.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kommunicator
{
    public class KommunicatorDbContext : DbContext
    {
        public DbSet<Administrator> Administrator { get; set; }

        public DbSet<Category> Category { get; set; }

        public DbSet<SubCategory> SubCategory {get; set;}

        public DbSet<Topic> Topic { get; set; }

        public DbSet<TopicSubscription> TopicSubscription { get; set; }

        public DbSet<Message> Message { get; set; }

        public DbSet<UserResponse> Response { get; set; }

        public DbSet<MessageTemplate> MessageTemplate { get; set; }

        public DbSet<ResponseTemplate> ResponseTemplate { get; set; }

        public DbSet<User> User { get; set; }

        public KommunicatorDbContext(DbContextOptions<KommunicatorDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TopicSubscription>().HasKey(ts => new { ts.TopicID, ts.UserEmail });

            // preventing cascade deleting when one tries to delete an object. We wont delete anything from the db, we use flags.
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.NoAction;
            }



        }
    }
}
