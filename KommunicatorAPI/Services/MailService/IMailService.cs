﻿using Kommunicator.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.MailService
{
    public interface IMailService
    {
        public Task<Response> SendOutgoingSet(EmailMessage emailMessage);

        public Task<Response> Deliver(SendGridMessage message);

        public List<EmailAddress> OrganizeSubscriberList(params string[] emails);
        public List<EmailAddress> OrganizeSubscriberList(List<string> emails);
    }
}
