using Kommunicator.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.CodeAnalysis.Options;
using Microsoft.EntityFrameworkCore.Internal;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Services.MailService
{
    public class MailService : IMailService
    {
        //Bruker og email det sendes fra, dette settes opp i sendgrid, dette må stemme
        private readonly EmailAddress From;
        private readonly SendGridClient client;

        public MailService()
        {
            string key = Environment.GetEnvironmentVariable("SENDGRID_API_KEY"); // ("NAME_OF_THE_ENVIRONMENT_VARIABLE_FOR_YOUR_SENDGRID_KEY");
            client = new SendGridClient("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");

            //From = new EmailAddress("andreas_myhr_93@hotmail.com", "Andreas");
            From = new EmailAddress("mathiastb13@gmail.com", "Kommunicator");
        }

        public async Task<Response> SendOutgoingSet(EmailMessage emailMessage)
        {
            var addresses = OrganizeSubscriberList(emailMessage.Emails);

            Response r = null;

            int i = 0;
            foreach(EmailAddress address in addresses)
            {
                SendGridMessage message = new SendGridMessage();
                message.SetFrom(From);
                message.AddTo(address);

                message.SetSubject(emailMessage.Subject);
                message.AddContent(MimeType.Text, "HTML didn't work -responseurl-");
                message.AddContent(MimeType.Html, emailMessage.Message.MessageTemplate.Content);

                message.AddSubstitution("-responseurl-", emailMessage.UniqueLinks[i]);
                i++;

                r = await Deliver(message);
            }

            return r;
        }

        public async Task<Response> Deliver(SendGridMessage message)
        {
            try
            {
                return await client.SendEmailAsync(message);
            }
            catch(Exception e)
            {
                throw new Exception("Failed to deliver emails: {0}", e);
            }
        }

        public List<EmailAddress> OrganizeSubscriberList(params string[] emails)
        {
            List<EmailAddress> subscribers = new List<EmailAddress>();

            int i = 1;
            foreach (string email in emails)
            {
                EmailAddress address = new EmailAddress(email, $"Email User {i}");
                subscribers.Add(address);
                i++;
            }

            return subscribers;
        }

        public List<EmailAddress> OrganizeSubscriberList(List<string> emails)
        {
            List<EmailAddress> subscribers = new List<EmailAddress>();

            int i = 1;
            foreach(string email in emails)
            {
                EmailAddress address = new EmailAddress(email, $"Email User {i}");
                subscribers.Add(address);
                i++;
            }

            return subscribers;
        }
    }
}